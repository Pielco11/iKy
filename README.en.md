# iKy

## PROJECT
Visit the Gitlab Page of the [Project](https://kennbroorg.gitlab.io/ikyweb/)

## INSTALL BACKEND

### Redis
You must install Redis
```shell
wget http://download.redis.io/redis-stable.tar.gz
tar xvzf redis-stable.tar.gz
cd redis-stable
make
```

And turn on the server in a terminal
```shell
redis-server
```

### Python stuff and Celery
You must install the requirements
```shell
pip install -r requirements.txt
```

And turn on Celery in another terminal, within the directory **backend**
```shell
./celery.sh
```

Finally, again, in another terminal turn on backend app from directory **backend** 
```shell
python app.py
```

## INSTALL FRONTEND

### Dependencies
First of all, install [nodejs](https://nodejs.org/en/)

And then you must install **gulp** and the dependencies from the directory **frontend**
```shell
cd frontend
npm install
```

Finally, to run frontend server, execute:
```shell
gulp serve
```

# ENJOY
Open the browser and [ENJOY](http://127.0.0.1:3000)

## CONFIG APIKEYS
For now please load the apiKeys of fullcontact and twitter through the **API Keys** option in the frontend.

# Coming soon
I promise to put everything together in a script.
